<?php

namespace App\DataProvider;

use App\Entity\Enterprise;

interface DataProviderInterface
{
    /**
     * Gets the available data for the given Enterprise using this DataProviderInterface.
     *
     * @param Enterprise $enterprise
     *
     * @return mixed
     */
    public function getData(Enterprise $enterprise);
}
