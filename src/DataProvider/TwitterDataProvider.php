<?php

namespace App\DataProvider;

use Abraham\TwitterOAuth\TwitterOAuth;
use App\DataProvider\Exception\DataProviderException;
use App\DataProvider\Exception\ResourceNotFoundException;
use App\Entity\Enterprise;

class TwitterDataProvider implements DataProviderInterface
{
    /** @var TwitterOAuth */
    private $twitter;

    /**
     * TwitterDataProvider constructor.
     *
     * @param string      $consumerKey
     * @param string      $consumerSecret
     * @param string|null $oauthToken
     * @param string|null $oauthTokenSecret
     */
    public function __construct($consumerKey, $consumerSecret, $oauthToken = null, $oauthTokenSecret = null)
    {
        $this->twitter = new TwitterOAuth($consumerKey, $consumerSecret, $oauthToken, $oauthTokenSecret);
        $this->twitter->setDecodeJsonAsArray(true);
    }

    /**
     * @param Enterprise $enterprise
     *
     * @throws DataProviderException
     *
     * @return array|mixed|object
     */
    public function getData(Enterprise $enterprise)
    {
        /** @var array $response */
        $response = $this->twitter->get('users/show', [
            'screen_name' => $enterprise->getTwitter(),
        ]);

        switch ($this->twitter->getLastHttpCode()) {
            case 200:
                return $response;
            case 404:
                throw new ResourceNotFoundException(
                    sprintf('Twitter account @%s was not found.', $enterprise->getTwitter())
                );
        }

        throw new DataProviderException(
            sprintf(
                'Twitter request for path "%s" on user %s returned a %d response code.',
                $this->twitter->getLastApiPath(),
                $enterprise->getTwitter(),
                $this->twitter->getLastHttpCode()
            )
        );
    }
}
