<?php

namespace App\Tests\DataProvider;

use App\DataProvider\TwitterDataProvider;
use App\Entity\Enterprise;
use PHPUnit\Framework\TestCase;

class TwitterDataProviderTest extends TestCase
{
    public function testGetData()
    {
        $username   = 'antoniocambados';
        $enterprise = new Enterprise();
        $provider   = new TwitterDataProvider(
            $_ENV['OAUTH_TWITTER_CONSUMER_KEY'],
            $_ENV['OAUTH_TWITTER_CONSUMER_SECRET'],
            $_ENV['OAUTH_TWITTER_ACCESS_TOKEN'] ?? null,
            $_ENV['OAUTH_TWITTER_ACCESS_TOKEN_SECRET'] ?? null
        );

        $enterprise->setTwitter($username);
        $data = $provider->getData($enterprise);

        $this->assertTrue(is_array($data));
        $this->assertArrayHasKey('screen_name', $data);
        $this->assertEquals($username, $data['screen_name']);
    }

    /**
     * @expectedException \App\DataProvider\Exception\DataProviderException
     */
    public function testGetDataInvalidCredentials()
    {
        $username   = 'antoniocambados';
        $enterprise = new Enterprise();
        $provider   = new TwitterDataProvider('foo', 'bar');

        $enterprise->setTwitter($username);
        $provider->getData($enterprise);
    }

    /**
     * @expectedException \App\DataProvider\Exception\ResourceNotFoundException
     */
    public function testGetDataInvalidUsername()
    {
        $username   = 'antoniocambados_foo';
        $enterprise = new Enterprise();
        $provider   = new TwitterDataProvider(
            $_ENV['OAUTH_TWITTER_CONSUMER_KEY'],
            $_ENV['OAUTH_TWITTER_CONSUMER_SECRET'],
            $_ENV['OAUTH_TWITTER_ACCESS_TOKEN'] ?? null,
            $_ENV['OAUTH_TWITTER_ACCESS_TOKEN_SECRET'] ?? null
        );

        $enterprise->setTwitter($username);
        $provider->getData($enterprise);
    }
}
