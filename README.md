Open Enterprise Explorer
==================

Proyecto cuya funcionalidad principal será la de recopilar datos de las empresas incluidas en el sistema. Basándose en la extracción de datos desde diferentes fuentes de datos. Como por ejemplo la integración de estas diferentes apis:

* Twitter ( número de seguidores )
* Instagram ( número de seguidores )
* Facebook ( número de likes en la fanpage )
* Linkedin ( número de empleados en el perfil de la empresa )
* Ranking de Alexa ( Número de visitas mensuales de su página web )


Será totalmente abierto y cualquier empresa podrá formar parte de este listado.
De cada empresa se sacará una información genérica sacada de las diferentes integraciones comentadas en el punto anterior y mayores cada empresa puede aportar los siguientes datos:

- Nombre de la empresa
- Elegir el sector al que pertenece del listado disponible
- Número de empleados dentro de los rankings disponibles
- Elegir las tecnologías que usa en su proyecto online
- Dirección / ciudad / comunidad

Proyectos similares
--------

Actualmente hay plataformas donde poder listar y consultar empresas de una cierta comunidad como puede ser el listado de clusterTic.

http://www.clustertic.net/empresas

Otro ejemplo es el proyecto de amtega: https://www.mapatic.gal/#provedores-tic

Contras: No tienen campos unificados, ni información actualizada de redes sociales. No permite buscar por categorias, ni compararlas por ciertos filtros comunes.

Otro ejemplo es https://stackshare.io/stackups

Esta plataforma nos permite comparar teconologías, pero está muy orientado unicamente a proyectos software

Un ejemplo muy parecido del objetivo de este proyecto sería la iniciativa de Barcelona http://w153.bcn.cat/#/explore

Es lo mas parecido, pero solo está orientado a la ciudad de Barcelona. No permite comparar o hacer un ranking por ciertos filtros.

La idea sería poder tener unos datos o gráficos de las empresas por comunidades y tecnologías como hace barcelona por sectores.
http://w153.bcn.cat/#/infographics


Instalación y puesta en marcha
========

Aquí tienes toda la documentación necesaria para poner este proyecto en marcha.
[doc/installation.md](doc/installation.md)
